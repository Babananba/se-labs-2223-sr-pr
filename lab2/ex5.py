import json

class Employee:
    def __init__(self, name, title, age, office):
        self.name = name
        self.title = title
        self.age = age
        self.office = office

    def __str__(self):
        return f"{self.name} ({self.age}), {self.title} @ {self.office}"


employees_list = []

with open("lab2/ex4-employees.json", "r") as file:
    for employee in json.load(file):
        employees_list.append(Employee(employee["employee"], employee["title"], employee["age"], employee["office"]))

for employee in employees_list:
    print(employee)
