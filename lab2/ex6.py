import json

class Employee:
    def __init__(self, name, title, age, office):
        self.name = name
        self.title = title
        self.age = age
        self.office = office

    def __str__(self):
        return f"{self.name} ({self.age}), {self.title} @ {self.office}"


class Company:
    def __init__(self, name):
        self.name = name
        self.employees = []

    def __str__(self):
        return f"{self.name} ({len(self.employees)} employees)"

    def employ(self, name, title, age, office):
        new_employee = Employee(name, title, age, office)
        self.employees.append(new_employee)

    def fire(self, name):
        for e in self.employees:
            if e.name == name:
                self.employees.remove(e)

    def load_from_json(self, path_to_json):
        try:
            with open(path_to_json) as file:
                for employee in json.load(file):
                    self.employees.append(Employee(employee["employee"], employee["title"], employee["age"], employee["office"]))
        except:
            print("JSON load error")

    def save_to_json(self, path_to_json):
        try:
            with open(path_to_json, "w") as file:
                result = []
                for employee in self.employees:
                    result.append({"employee": employee.name, "title": employee.title, "age": employee.age, "office": employee.office})
                json.dump(result, file)
        except:
            print("JSON dump error")

    def print_employees(self):
        print(self.name)
        print("----------------")
        for i in range(len(self.employees)):
            print(f"{i + 1}. {self.employees[i]}")


def main():
    nike = Company("Nike")
    print(nike)

    nike.employ("Homer Simpson", "CEO", 44, "Lobby")
    nike.employ("Marge Simpson", "CTO", 33, "Lobby")
    print(nike)

    nike.fire("Homer Simpson")
    print(nike)

    # Implement load_from_json, save_to_json and print_employees methods
    # Then uncomment the implemented methods
    adidas = Company("Adidas")
    adidas.load_from_json("lab2/ex4-employees.json")
    
    # After loading from json, adidas should have all the employees from
    # json file
    print(adidas)

    # Print employees should now print all the employees
    adidas.print_employees()

    adidas.employ("Homer Simpson", "CEO", 44, "Lobby")
    adidas.employ("Marge Simpson", "CTO", 33, "Lobby")
    adidas.employ("Bart Simpson", "CEO", 44, "Lobby")
    adidas.employ("Lisa Simpson", "CTO", 33, "Lobby")
    print(adidas)
    adidas.print_employees()

    adidas.fire("Homer Simpson")
    adidas.fire("Marge Simpson")
    print(adidas)
    adidas.print_employees()

    # Saving employees db to a new file, the file should now have 2 more
    # employees (Bart and Lisa, since Homer and Marge were fired)
    adidas.save_to_json("lab2/ex6-employees.json")

if __name__ == "__main__":
    main()

