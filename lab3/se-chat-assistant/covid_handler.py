from email import header
from query_handler_base import QueryHandlerBase
import random
import requests
import json

class CovidHandler(QueryHandlerBase):
    def can_process_query(self, query):
        return "covid" in query
        pass

    def process(self, query):

        try:
            result = self.call_api()
            totalCases = result[0]["TotalCases"]
            self.ui.say(f"Total cases in the world: {totalCases}")
        except: 
            self.ui.say("Oh no! There was an error trying to contact VACCOVID api.")
            self.ui.say("Try something else!")
        pass

    def call_api(self):
        url = "https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/npm-covid-data/world"
        headers = {
            "X-RapidAPI-Key": "c79bddae15msh5d91edf819ba4d8p196b6bjsn8024ba170d64",
            "X-RapidAPI-Host": "vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers)
        
        return json.loads(response.text)
