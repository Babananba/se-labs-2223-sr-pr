def draw_dash(size):
    print(" " + "--- " * size)

def draw_bars(size):
    print("|" + " |" * size)

if __name__ == "__main__":
    size = int(input("What size of game board? "))
for rows in range(size):
    draw_dash(size)
    draw_bars(size)
    draw_dash(size)