def contains_two_twos(list):
    """Return the True if list contains 2 next to a 2 somewhere, otherwise False

    Arguments:
    data - list of numbers
    """

    lastWasTwo = False

    for val in list:
        if val == 2:
            if lastWasTwo:
                return True
            lastWasTwo = True
        else:
            lastWasTwo = False
    
    return False


data = input("Input a list: ").split(",")

try:
    data = [int(val) for val in data]
except:
    print("Bad data")

print(data)
print(contains_two_twos(data))
