def count_evens(data):
    """Return the number of even numbers inside a list

    Arguments:
    data - list of integers
    """

    count = 0

    for val in data:
        if val % 2 == 0:
            count += 1

    return count

def main():
    lists = [
        [1, 2, 3, 4, 5], 
        [7, 8, 9, 10, 31, 52, 78],
    ]

    for l in lists:
        print(f"List {l} has {count_evens(l)} even numbers")

if __name__ == "__main__":
    main()
