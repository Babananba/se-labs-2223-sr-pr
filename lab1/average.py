def get_average(data):
    """Return the average of a list

    Arguments:
    data - list of numbers
    """

    sum = 0

    for val in data:
        sum += val

    return sum / len(data)

def main():
    lists = [
        [1, 2, 3, 4, 5], 
        [7, 8, 9, 10, 31, 52, 78],
    ]

    for l in lists:
        print(f"Average of list {l} is {get_average(l)}")

if __name__ == "__main__":
    main()

