def char_types_count(data):
    """Returns tuple of type counts

    Arguments:
    data - list of data
    """

    res = [0, 0, 0]

    for val in data:
        if val.islower():
            res[0] += 1
        elif val.isnumeric():
            res[1] += 1
        elif val.isupper():
            res[2] += 1

    return (res[0], res[1], res[2])

data = "wasd123POL"

print(data)
print(char_types_count(data))

